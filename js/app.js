$(function() {

	var App = (function(){

		return {
			init : function() {


				Detect.init();
				Templating.init();
			}
		}
	})()
		,Slides = (function(){
			var selectSlide = function(i){
				$texts.find(".app-texts-i[data-slide='" + i + "']").addClass('active');
				$content.addClass('on');
				$(".app-blocks").find(".app-blocks-i[data-slide='" + i + "']").addClass('active');
				$photos.find("[data-slick-index='"+ i +"']").addClass('slick-current');
			};
		var unset = function(){
			$texts.find(".active").removeClass('active');
			$content.removeClass('on');
			$(".app-blocks-i.active").removeClass('active');
		};
		var $content = $('#app-content');
		var $texts = $('#texts-slider');
		var $photos = $('#app-slides');
			return {
				init : function() {

					$photos.slick({
						slidesToShow: 10,
						slidesToScroll: 1,
						arrows: false,
						focusOnSelect: true,
					});
					$photos.find(".app-slide.slick-current").removeClass('slick-current');
					$photos.find(".app-slide").on("click", function(){
						unset();
						var inFocus = +$photos.find(".slick-current").attr("data-slick-index");
						$content.addClass('on');
						$texts.find(".app-texts-i[data-slide='" + inFocus + "']").addClass('active');
						$(".app-blocks").find(".app-blocks-i[data-slide='" + inFocus + "']").addClass('active');
					});
					$('.app-blocks-i .close').on("click",function(e){
						e.preventDefault();
						unset();
						$photos.find(".app-slide.slick-current").removeClass('slick-current');
					});
					$(document).on("click",".point",function(e){
						e.preventDefault();
						var inFocus = +$(this).attr("data-slide");
						selectSlide(inFocus);
					});
					$('.close-all').on("click",function(e){
						e.preventDefault();
						unset();
						$photos.find(".app-slide.slick-current").removeClass('slick-current');
					});
				}
			}
		})()

			,Detect = (function(){
				var oldies = {
					'chrome': 15,
					'msie' : 10,
					'firefox': 15,
					'safari': 5
				};
				return {
					init : function() {

						var name = BrowserDetect.browser.name,
								ver = BrowserDetect.browser.value,
								isOld = false;
						$.each(oldies, function(key, val){
							if (key == name){
								if (ver.split('.')[0] < val){
									isOld = true;
								}
							}
						});
						isOld ? $('#app-wrap').addClass('oldie') : '' ;
					}
				}
			})()
			,Templating = (function(){
				var data = {
					items: [
						{img: 'images/slides/s1.jpg'},
						{img: 'images/slides/s2.jpg'},
						{img: 'images/slides/s3.jpg'},
						{img: 'images/slides/s4.jpg'},
						{img: 'images/slides/s5.jpg'},
						{img: 'images/slides/s6.jpg'},
						{img: 'images/slides/s7.jpg'},
						{img: 'images/slides/s8.jpg'},
						{img: 'images/slides/s9.jpg'},
						{img: 'images/slides/s10.jpg'},
					],
					index: function() {
						return ++window['INDEX']||(window['INDEX']=0);
					}
				};
				var buildPoints = function(){
					var slidesCount = data.items.length;
					var result = '';
					for (var i=0;i<slidesCount;i++){
						var point = "<div class='point' data-slide='" + i + "'><i class='icon icon-plus'></i></div>";
						if (i == 5){
							result += point;
						}
						result += point;
					}
					$("#app-points").append(result);

					$("#app-points").find(".point").addClass("animated bounceInDown");
					//$("#app-points").on("animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd",".point",function(){
					//	//
					//});
				};
				return {
					init : function() {
						var templateBuild = function(){
							var output = $("#app-slides");
							var template = $("#slides-template").html();
							var html = Mustache.render(template, data);
							output.html(html);
						};
						templateBuild();
						buildPoints();
						Slides.init();
					}
				}
			})()
	/**
	/**
	 * Dummy Module Example
	 */
		,DummyModule = (function(){
			return {
				init : function() {
					// do something
				}
			}
		})()

		;App.init();

});

